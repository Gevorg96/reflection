package be.kdg.distrib.skeletonFactory;

import be.kdg.distrib.stubFactory.StubInvocationHandler;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class SkeletonFactory {
    private Skeleton skeleton;
    public SkeletonFactory() {
    }


    public static Object createSkeleton(Object testImplementation) {

        System.out.println(testImplementation.getClass().getInterfaces()[0]);
        InvocationHandler handler = new SkeletonInvocationHandler(testImplementation);
        Object result = Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[]
                {Skeleton.class}, handler);
        return  result;

    }
}
