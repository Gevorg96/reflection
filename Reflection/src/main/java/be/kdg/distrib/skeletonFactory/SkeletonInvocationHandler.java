package be.kdg.distrib.skeletonFactory;

import be.kdg.distrib.communication.MessageManager;
import be.kdg.distrib.communication.MethodCallMessage;

import java.lang.reflect.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class SkeletonInvocationHandler implements InvocationHandler {
    private Object implementation;
    private MessageManager messageManager;
    SkeletonInvocationHandler(Object implementation
    ) {
        this.implementation =  implementation;
        this.messageManager = new MessageManager();

    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        if (method.getName().equals("run")) {
            new Thread(() ->
            {
                while (true) {
                        MethodCallMessage request = this.messageManager.wReceive();
                    try {
                        handelRequest(request);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    } catch (InstantiationException e) {
                        e.printStackTrace();
                    }

                }

            }).start();

        }

        if (method.getName().equals("getAddress")) {
            return messageManager.getMyAddress();
        }

        if (method.getName().equals("handleRequest")) {
            MethodCallMessage message = (MethodCallMessage) args[0];
            handelRequest(message);
        }
        return null;
    }


        private void handelRequest (MethodCallMessage message) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, InstantiationException {

            List<Method> temp = Arrays.asList(implementation.getClass().getDeclaredMethods());
            //method die binnen komt moet ook gevonden worden in de opbject waarme ze Skeleton maken
            Method foundMethod = temp.stream().filter(f -> f.getName().equals(message.getMethodName())  ).findFirst().get();


            //HashSet maken van parameters die gevonden worden bij message die binnenkomt
            //Dit teel ik door arg0 ... arg10 te tellen
            //als een arg object is neem ik het maar een keer dus als arg1.naam arg1.familienaam neem ik maar een keer in plaats van 2 keer
            HashSet<String> params  = new HashSet<>();
            //gevonden method en zijn parameters controlleren
            for (String param: message.getParameters().keySet()) {
                if(param.contains(".")){
                    //bestaat dit arg[nmr] al in onze HashSet? als niet voeg dit toe
                    if(!(params.contains(param.split("\\.")[0]))){
                        params.add(param.split("\\.")[0]);
                    };
                }else {
                    params.add(param);
                }
            }
            //Parametes zijn fout meegegeven gooi een verwachtte RuntimeException
            if (foundMethod.getParameterCount() != params.size()){
                throw new RuntimeException("Parametes zijn niet gelijk");
            }

            //gevonden methode invoken (laten lopen)
            Object[] methodArguments = new Object[foundMethod.getParameterCount()];
            for(int i = 0 ; i  <  foundMethod.getParameterCount(); i++) {
                if(message.getParametersStartingWith("arg" + i).size() == 0) {
                    throw new RuntimeException("argument name is not expeted name");
                }
                Class type = foundMethod.getParameterTypes()[i];
                if (type == String.class) { methodArguments[i] =  message.getParameter("arg" +i);}
                else  if (type == Integer.TYPE) { methodArguments[i] =  Integer.parseInt( message.getParameter("arg" +i)) ;}
                else  if (type == Character.TYPE){ methodArguments[i] = message.getParameter("arg" +i).charAt(0);}
                else  if (type == Float.TYPE){ methodArguments[i] = Float.parseFloat( message.getParameter("arg" +i)); }
                else  if (type == Byte.TYPE){ methodArguments[i] = Byte.parseByte( message.getParameter("arg" +i)); }
                else  if (type == Boolean.TYPE){ methodArguments[i] = Boolean.parseBoolean( message.getParameter("arg" +i)); }
                else  if (type == Double.TYPE){ methodArguments[i] = Double.parseDouble( message.getParameter("arg" +i)); }
                else{
                    Object object = type.getDeclaredConstructor().newInstance();
                    for (Field fild : type.getDeclaredFields()) {
                        fild.setAccessible(true);
                        if (fild.getType() == String.class) { fild.set( object, message.getParameter("arg" + i + "." +fild.getName()) );}
                        else  if (fild.getType() == Integer.TYPE) { fild.set( object,  Integer.parseInt(message.getParameter("arg" + i + "."+fild.getName()))) ;}
                        else  if (fild.getType() == Character.TYPE){ fild.set( object, message.getParameter("arg" + i + "."+fild.getName()).charAt(0)); }
                        else  if (fild.getType() == Float.TYPE){ fild.set( object, Float.parseFloat( message.getParameter("arg" + i + "."+fild.getName()))); }
                        else  if (fild.getType() == Byte.TYPE){ fild.set( object, Byte.parseByte( message.getParameter("arg" + i + "."+fild.getName()))); }
                        else  if (fild.getType() == Boolean.TYPE){ fild.set( object, Boolean.parseBoolean( message.getParameter("arg" + i + "."+fild.getName()))); }
                        else  if (fild.getType() == Double.TYPE){ fild.set( object, Double.parseDouble( message.getParameter("arg" + i + "."+fild.getName()))); }
                    }
                    methodArguments[i] = object;
                }
            }
            //return dat normaal onze methode moet sturen
            //gevonden methode invoken (laten lopen)
            Object result =  foundMethod.invoke(implementation, methodArguments);

            //reply klaarzetten om naar de Stub te sturen
            MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(),foundMethod.getName());

            //kijken naar de returntype van de methode als dat void is stuur okay
            if (foundMethod.getReturnType() == Void.TYPE) {
                reply.setParameter("result" , "Ok");
            }
            //als dat primitive type is stuur je dan gwn de waarde dat invoke heeft truggegeven aan ons.
            else if (foundMethod.getReturnType() == String.class
                    || foundMethod.getReturnType()  == Integer.TYPE
                    || foundMethod.getReturnType()  == Character.TYPE
                    || foundMethod.getReturnType() == Long.TYPE
                    || foundMethod.getReturnType() == Float.TYPE
                    || foundMethod.getReturnType()  == Boolean.TYPE
                    || foundMethod.getReturnType()  == Byte.TYPE
                    || foundMethod.getReturnType()  == Double.TYPE
            ) {
                reply.setParameter("result" , result.toString());
                //als return type een object is moet je dan de filds van dat object goed zetten
                // ( meerdere parameters maken op reply ) en dit doorsturen.
            }else {
                for (Field fild : foundMethod.getReturnType().getDeclaredFields()) {
                    fild.setAccessible(true);
                    if (fild.getType() == String.class
                            || fild.getType() == Integer.TYPE
                            || fild.getType()  == Character.TYPE
                            || fild.getType() == Long.TYPE
                            || fild.getType() == Float.TYPE
                            || fild.getType()  == Boolean.TYPE
                            || fild.getType()  == Byte.TYPE
                            || fild.getType()  == Double.TYPE
                    ) {
                        reply.setParameter("result." + fild.getName() , fild.get(result).toString());
                    }

                }

            }
            messageManager.send(reply,message.getOriginator());
        }
    }


