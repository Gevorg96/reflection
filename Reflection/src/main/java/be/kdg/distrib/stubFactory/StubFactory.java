package be.kdg.distrib.stubFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class StubFactory {
    public StubFactory() {
    }
    public static Object createStub(Class  stubClass , String s, int port) {
        InvocationHandler handler = new StubInvocationHandler( s,  port);
        Object result = Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[]
                {stubClass}, handler);
        return  result;
    }
}
