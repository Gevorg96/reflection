package be.kdg.distrib.stubFactory;

import be.kdg.distrib.communication.MessageManager;
import be.kdg.distrib.communication.MethodCallMessage;
import be.kdg.distrib.communication.NetworkAddress;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;



public class StubInvocationHandler implements InvocationHandler {
    private String adress;
    private int port;
    private MessageManager messageManager;
    private NetworkAddress networkAddress;

    public StubInvocationHandler(String adress, int port) {
        this.adress = adress;
        this.port = port;
        this.messageManager = new MessageManager();
        this.networkAddress = new NetworkAddress(this.adress, this.port);
    }

    @Override
    //method(in ksdfjk , string isets)
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        MethodCallMessage callSkeleton = new MethodCallMessage(messageManager.getMyAddress(), method.getName());
        
        //als methode die opgeroepen word primitieve parametersTypes heeft maak  MethodCallMessage en geef parameters in
        for (int i = 0; i < method.getParameterCount(); i++) {
            if (method.getParameterTypes()[i] == String.class
                    || method.getParameterTypes()[i] == Integer.TYPE
                    || method.getParameterTypes()[i] == Character.TYPE
                    || method.getParameterTypes()[i] == Long.TYPE
                    || method.getParameterTypes()[i] == Float.TYPE
                    || method.getParameterTypes()[i] == Double.TYPE
                    || method.getParameterTypes()[i] == Boolean.TYPE
                    || method.getParameterTypes()[i] == Byte.TYPE
            ) {
                callSkeleton.setParameter("arg" + i, args[i].toString());
            } else {
                //non primitive parameters (objects)
                for (Field fild : method.getParameterTypes()[i].getDeclaredFields()) {
                    fild.setAccessible(true);
                    if (fild.getType() == String.class
                            || fild.getType() == Integer.TYPE
                            || fild.getType() == Character.TYPE
                            || fild.getType() == Long.TYPE
                            || fild.getType() == Float.TYPE
                            || fild.getType() == Boolean.TYPE
                            || fild.getType() == Byte.TYPE
                            || fild.getType()  == Double.TYPE
                    ) {
                        //als je de value van een fild wil moet je Object megeven om de value van die fild IN Dat OBJECT TE VERKRIJGEN
                        callSkeleton.setParameter("arg" + i + "." + fild.getName(), fild.get(args[i]).toString());
                    }
                    //als filds van dat object objects zijn negeeren we het
                }
            }
        }
            //als je al parameters hebt gezet moet je doorsturen naar dit networdkAdress
            this.messageManager.send(callSkeleton,this.networkAddress);

            //return van de methode met # parameters die we doorgestuurd hebben ontvangen en handelen
            MethodCallMessage respons = messageManager.wReceive();

            if (method.getReturnType() == Void.TYPE) {
                if (respons.getParameter("result").equals("Ok")) {
                    return null;
                } else {throw new RuntimeException("Status van Server is niet goed");}
            }else  if (method.getReturnType() == String.class) {return respons.getParameter("result") ;}
            else  if (method.getReturnType() == Integer.TYPE) {return  Integer.parseInt(respons.getParameter("result")) ;}
            else  if (method.getReturnType() == Character.TYPE){return respons.getParameter("result").charAt(0); }
            else  if (method.getReturnType() == Float.TYPE){return Float.parseFloat( respons.getParameter("result")); }
            else  if (method.getReturnType() == Byte.TYPE){return Byte.parseByte( respons.getParameter("result")); }
            else  if (method.getReturnType() == Boolean.TYPE){return Boolean.parseBoolean( respons.getParameter("result")); }
            else  if (method.getReturnType() == Double.TYPE){return Double.parseDouble( respons.getParameter("result")); }

            else {
                Object object = method.getReturnType().getDeclaredConstructor().newInstance();
                for (Field fild : method.getReturnType().getDeclaredFields()) {
                    fild.setAccessible(true);
                    if (fild.getType() == String.class) { fild.set( object, respons.getParameter("result."+fild.getName()) );}
                    else  if (fild.getType() == Integer.TYPE) { fild.set( object,  Integer.parseInt(respons.getParameter("result."+fild.getName()))) ;}
                    else  if (fild.getType() == Character.TYPE){ fild.set( object, respons.getParameter("result."+fild.getName()).charAt(0)); }
                    else  if (fild.getType() == Float.TYPE){ fild.set( object, Float.parseFloat( respons.getParameter("result."+fild.getName()))); }
                    else  if (fild.getType() == Byte.TYPE){ fild.set( object, Byte.parseByte( respons.getParameter("result."+fild.getName()))); }
                    else  if (fild.getType() == Boolean.TYPE){ fild.set( object, Boolean.parseBoolean( respons.getParameter("result."+fild.getName()))); }
                    else  if (fild.getType() == Double.TYPE){ fild.set( object, Double.parseDouble( respons.getParameter("result."+fild.getName()))); }
                }
                return object;
            }
    }
}

